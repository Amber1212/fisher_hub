from django.http import HttpResponseForbidden
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, update_session_auth_hash, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from .forms import UserRegistrationForm, UserLoginForm, PhotoForm, DescriptionForm
from .models import UserProfile
from django.contrib import messages
from django.shortcuts import get_object_or_404


def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = UserRegistrationForm()
    return render(request, 'registration/register.html', {'form': form})


def user_login(request):
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.error(request, 'Invalid username or password.')
    else:
        form = UserLoginForm()
    return render(request, 'registration/login.html', {'form': form})


def home(request):
    return render(request, 'main/home.html')


@login_required
def edit_profile(request):
    user = request.user
    profile, created = UserProfile.objects.get_or_create(username=user)
    user_form = UserRegistrationForm(instance=user)
    photo_form = PhotoForm(instance=profile)
    description_form = DescriptionForm(instance=profile)

    if request.method == 'POST':
        action = request.POST.get('action')
        if action == 'change_basic_info':
            user_form = UserRegistrationForm(request.POST, instance=user)
            if user_form.is_valid():
                user_form.save()
                messages.success(request, 'Основні дані успішно змінено!')
                return redirect('home')
        elif action == 'change_photo':
            photo_form = PhotoForm(request.POST, request.FILES, instance=profile)
            if photo_form.is_valid():
                photo_form.save()
                messages.success(request, 'Фото успішно змінено!')
                return redirect('home')
        elif action == 'change_description':
            description_form = DescriptionForm(request.POST, instance=profile)
            if description_form.is_valid():
                description_form.save()
                messages.success(request, 'Опис профілю успішно змінено!')
                return redirect('home')

    context = {
        'user_form': user_form,
        'photo_form': photo_form,
        'description_form': description_form,
    }
    return render(request, 'registration/edit_profile.html', context)


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your password has been successfully changed!')
            return redirect('home')
        else:
            messages.error(request, 'Please correct the error.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/change_password.html', {'form': form})


@login_required
def user_logout(request):
    logout(request)
    return redirect('home')


@login_required
def profile(request):
    user = request.user
    if not request.user.has_perm('main.can_view_profile', user):
        return HttpResponseForbidden("Нема доступу!")

    profile = get_object_or_404(UserProfile, username=user.username)

    return render(request, 'main/profile.html', {'user': profile})