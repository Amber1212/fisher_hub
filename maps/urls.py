from django.urls import path
from .views import AddressListView, AddressCreate, AddressView, save_mark

urlpatterns = [
    path('add_mark/', AddressCreate.as_view(), name='add_mark'),
    path('save_mark/', save_mark, name='save_mark'),
    path('show_marks/', AddressListView.as_view(), name='show_marks'),
    path('water_info/<int:pk>/', AddressView.as_view(), name='water_info'),

]
