# Generated by Django 5.0.2 on 2024-03-16 17:03

import django.db.models.deletion
import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("fish_info", "0001_initial"),
        ("maps", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="WaterType",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("water_type", models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name="address",
            name="description",
            field=models.TextField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name="address",
            name="fish",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="fish_info.fish",
            ),
        ),
        migrations.AddField(
            model_name="address",
            name="water_description",
            field=models.TextField(default=""),
        ),
        migrations.AddField(
            model_name="address",
            name="water_type",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="maps.watertype",
            ),
        ),
    ]
