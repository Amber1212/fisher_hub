from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.views.generic import CreateView, ListView, DetailView
from django.shortcuts import redirect
from .forms import CommentForm
from .models import Address, WaterType
from fish_info.models import Fish
from django.utils.translation import gettext as _

mapbox_api_key = "pk.eyJ1IjoiaWxsaWRhbjEyMyIsImEiOiJjbHVncWhvNm0yYXRoMmhxdTJyeGtvMnBuIn0.tn9RZUkv1QpKb3YehpYH8Q"


@require_POST
@login_required
def save_mark(request):
    """Збереження доданої водойми"""
    if request.method == 'POST':
        water_name = request.POST.get('water_name')
        water_type_name = request.POST.get('water_type')
        water_description = request.POST.get('water_description')
        longitude_str = request.POST.get('longitude')
        latitude_str = request.POST.get('latitude')
        fish_names = request.POST.getlist('fish')
        price_str = request.POST.get('price')

        # Функція для конвертації коми в крапку
        def convert_decimal_str(decimal_str):
            return decimal_str.replace(',', '.') if decimal_str else None

        # Конвертація координат і ціни з коми в крапку
        longitude = convert_decimal_str(longitude_str)
        latitude = convert_decimal_str(latitude_str)
        price = convert_decimal_str(price_str)

        if all([water_name, water_type_name, water_description, longitude, latitude, fish_names, price]):
            try:
                water_type, created = WaterType.objects.get_or_create(water_type=water_type_name)
                address = Address.objects.create(
                    water_name=water_name,
                    water_type=water_type,
                    water_description=water_description,
                    lat=latitude,
                    long=longitude,
                    price=price
                )
                # Відображення всіх існуючих об'єктів риб
                fish_objects = Fish.objects.filter(name__in=fish_names)
                address.fish.set(fish_objects)
                return JsonResponse({'success': True})
            except Exception as e:
                return JsonResponse({'success': False, 'error': str(e)})
        else:
            return JsonResponse({'success': False, 'error': _('Потрібно надати всі дані')})
    else:
        return JsonResponse({'success': False, 'error': _('Метод запиту має бути POST')})


class AddressCreate(CreateView):
    """Відображення карти на якій можна додавати адресу"""
    model = Address
    fields = ['water_name', 'water_type', 'water_description']
    template_name = 'maps/add_water.html'
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['access_token'] = mapbox_api_key

        context['fish_names'] = Fish.objects.values_list('name', flat=True)
        return context


class AddressListView(ListView):
    """Відображення доданих адрес на карті"""
    model = Address
    template_name = 'maps/show_marks.html'
    context_object_name = 'addresses'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['access_token'] = mapbox_api_key
        context['fish_names'] = Fish.objects.values_list('name', flat=True)
        return context

    def get_queryset(self):
        return Address.objects.all()


class AddressView(DetailView):
    model = Address
    template_name = 'maps/address_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comment_form'] = CommentForm()
        return context

    def post(self, request, pk):
        address = Address.objects.get(pk=pk)
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.address = address
            comment.save()
            return redirect('water_info', pk=pk)
        else:
            return self.get(request, pk)
