from django.contrib import admin
from .models import Address, WaterType


admin.site.register(Address)
admin.site.register(WaterType)
