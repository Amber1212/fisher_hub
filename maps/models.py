from django.contrib.auth.models import User
from django.db import models
from fish_info.models import Fish


class WaterType(models.Model):
    """Модель типу водойми"""
    water_type = models.CharField(max_length=100)

    def __str__(self):
        return self.water_type


class Address(models.Model):
    """Модель адреси водойми"""
    water_name = models.TextField()
    water_type = models.ForeignKey(WaterType, on_delete=models.SET_NULL, null=True, blank=True)
    water_description = models.TextField(default='')
    price = models.CharField(max_length=20, blank=True, null=True)
    fish = models.ManyToManyField(Fish, blank=True)
    lat = models.FloatField(blank=True, null=True)
    long = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.water_name


class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Comment by {self.author.username} on {self.address.water_name}'
