from django import forms


class FishSearchForm(forms.Form):
    title = forms.CharField(max_length=255,
                            required=False,
                            label="",
                            widget=forms.TextInput(
                                attrs={'placeholder': "Шукати по назві"}
                            )
                            )


class GlobalSearchForm(forms.Form):
    title = forms.CharField(max_length=255,
                            required=False,
                            label="",
                            widget=forms.TextInput(
                                attrs={'placeholder': "Шукати на сайті"}
                            )
                            )