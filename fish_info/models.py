from django.db import models


class FishClassification(models.Model):
    """Модель для класифікації риб"""
    classification = models.CharField(max_length=75)

    def __str__(self):
        return self.classification


class FishFamily(models.Model):
    """Модель для сімейства риб"""
    family = models.CharField(max_length=75)

    def __str__(self):
        return self.family


class FishDiet(models.Model):
    """Модель для опису харчування риб"""
    diet = models.CharField(max_length=55)

    def __str__(self):
        return self.diet


class Fish(models.Model):
    """Основна модель яка має інформацію з попередніх класів, а також свою унікальну"""
    img = models.ImageField()
    name = models.CharField(max_length=100)
    fish_classification = models.ForeignKey(FishClassification, on_delete=models.CASCADE)
    fish_family = models.ForeignKey(FishFamily, on_delete=models.CASCADE)
    fish_diet = models.ForeignKey(FishDiet, on_delete=models.CASCADE)
    description = models.TextField()

    def __str__(self):
        return self.name
