# Generated by Django 5.0.2 on 2024-03-07 19:01

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="FishClassification",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("classification", models.CharField(max_length=75)),
            ],
        ),
        migrations.CreateModel(
            name="FishDiet",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("diet", models.CharField(max_length=55)),
            ],
        ),
        migrations.CreateModel(
            name="FishFamily",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("family", models.CharField(max_length=75)),
            ],
        ),
        migrations.CreateModel(
            name="Fish",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("img", models.ImageField(upload_to="")),
                ("name", models.CharField(max_length=100)),
                ("description", models.TextField()),
                (
                    "fish_classification",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="fish_info.fishclassification",
                    ),
                ),
                (
                    "fish_diet",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="fish_info.fishdiet",
                    ),
                ),
                (
                    "fish_family",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="fish_info.fishfamily",
                    ),
                ),
            ],
        ),
    ]
