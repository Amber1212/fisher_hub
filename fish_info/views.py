from django.shortcuts import render
from django.views import View
from .models import Fish, FishDiet, FishFamily, FishClassification
from .forms import FishSearchForm

class FishSearchView(View):
    """Клас для реалізації пошуку за назвою риби"""

    def get(self, request):
        query = request.GET.get('query')
        if query:
            fishes = Fish.objects.filter(name__icontains=query)
        else:
            fishes = Fish.objects.all()

        search_form = FishSearchForm(request.GET)
        return render(request, 'fish_details/fish_list.html', {'fishes': fishes, 'search_form': search_form})


class ClassificationListView(View):
    """Клас для відображення списку класифікацій риби"""

    def get(self, request):
        classification = FishClassification.objects.all()
        return render(request, 'fish_details/classification_list.html', {'classification': classification})


class ClassificationDetailView(View):
    """Клас для відображення деталей класифікацій риб"""

    def get(self, request, classification_id):
        classification = FishClassification.objects.get(id=classification_id)
        fishes = Fish.objects.filter(fish_classification=classification)
        return render(request, 'fish_details/classification_detail.html', {'classification': classification, 'fishes': fishes})


class FamilyListView(View):
    """Клас для відображення списку сімейства риб"""

    def get(self, request):
        family = FishFamily.objects.all()
        return render(request, 'fish_details/family_list.html', {'family': family})


class FamilyDetailView(View):
    """Клас для відображення деталей за віднощенням до сімейства"""

    def get(self, request, family_id):
        family = FishFamily.objects.get(id=family_id)
        fishes = Fish.objects.filter(fish_family=family)
        return render(request, 'fish_details/family_detail.html', {'family': family, 'fishes': fishes})


class DietListView(View):
    """Клас для відображення списку харчування риб"""

    def get(self, request):
        diet = FishDiet.objects.all()
        return render(request, 'fish_details/diet_list.html', {'diet': diet})


class DietDetailView(View):
    """Клас для відображення які риби належать до обраного типу харчування"""

    def get(self, request, diet_id):
        diet = FishDiet.objects.get(id=diet_id)
        fishes = Fish.objects.filter(fish_diet=diet)
        return render(request, 'fish_details/diet_detail.html', {'diet': diet, 'fishes': fishes})


class FishLibraryView(View):
    """Клас для відображення бібліотеки"""

    def get(self, request):
        classifications = FishClassification.objects.all()
        families = FishFamily.objects.all()
        diets = FishDiet.objects.all()
        return render(request, 'main/fish_library.html', {'classifications': classifications, 'families': families, 'diets': diets})


class FishListView(View):
    """Клас для відображення списку усіх доступних риб"""

    def get(self, request):
        fishes = Fish.objects.all()
        search_form = FishSearchForm()
        return render(request, 'fish_details/fish_list.html', {'fishes': fishes, 'search_form': search_form})


class FishDetailView(View):
    """Клас для відображення деталей обраної риби"""

    def get(self, request, pk):
        fish = Fish.objects.get(pk=pk)
        return render(request, 'fish_details/fish_detail.html', {'fish': fish})


class GlobalSearchView(View):
    def get(self, request):
        query = request.GET.get('query')
        if query:
            fish_results = Fish.objects.filter(name__icontains=query)
            diet_results = FishDiet.objects.filter(diet__icontains=query)
            family_results = FishFamily.objects.filter(family__icontains=query)
            classification_results = FishClassification.objects.filter(classification__icontains=query)
            search_results = {
                'fish_results': fish_results,
                'diet_results': diet_results,
                'family_results': family_results,
                'classification_results': classification_results,
            }
        else:
            search_results = FishSearchForm(request.GET)
        return render(request, 'main/search_results.html', {'search_results': search_results})

