from django.contrib import admin
from .models import Fish, FishDiet, FishFamily, FishClassification


admin.site.register(Fish)
admin.site.register(FishDiet)
admin.site.register(FishFamily)
admin.site.register(FishClassification)
