from django.urls import path
from . import views

urlpatterns = [
    path('fish/', views.FishListView.as_view(), name='fish_list'),
    path('fish/<int:pk>/', views.FishDetailView.as_view(), name='fish_detail'),
    path('classification/', views.ClassificationListView.as_view(), name='classification_list'),
    path('classification/<int:classification_id>/', views.ClassificationDetailView.as_view(),
         name='classification_detail'),
    path('family/', views.FamilyListView.as_view(), name='family_list'),
    path('family/<int:family_id>/', views.FamilyDetailView.as_view(), name='family_detail'),
    path('diet/', views.DietListView.as_view(), name='diet_list'),
    path('diet/<int:diet_id>/', views.DietDetailView.as_view(), name='diet_detail'),
    path('fish_library/', views.FishLibraryView.as_view(), name='fish_library'),
    path('fish/search/', views.FishSearchView.as_view(), name='fish_search'),
    path('search/', views.GlobalSearchView.as_view(), name='global_search'),

]
